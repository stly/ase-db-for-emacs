;;; ase-db --- Use ase-db in GNU Emacs

;; Copyright (C) 2015 Steen Lysgaard

;; Author: Steen Lysgaard <st.lysgaard@gmail.com>
;; URL: Nowhere
;; Keywords: ase-db, Atomic Simulation Environment

;;; Commentary:
;;
;; Start it with M-x ase-db
;; then you will get prompted for the location of the db file.
;;
;; Enjoy!

;;; Code:

(require 'tabulated-list)
(setq-local lexical-binding t)

(defun ase-db (address &optional sel opts)
  "The main function.
ADDRESS is the location of the db file.
SEL is the selection.
OPTS is the options."
  (interactive "fdb file:\nsSelection (optional):\nsOptions (optional): ")
  (switch-to-buffer-other-window (concat "ase-db " address))
  (setq ase-db-location address)
  (ase-db-pre-process)
  (ase-db-set-all-keywords)

  (ase-db-mode)
  
  (tabulated-list-print t)
  (add-hook 'tabulated-list-revert-hook 'ase-db-mode)
  )

(defun ase-db-pre-process ()
  (setq db-full (split-string (do-ase-db ase-db-location ase-db-selection "++") "\n"))
  (setq ase-db-all-list-entries
		(get-all-list-entries (butlast (cdr db-full) 2)))
  (setq ase-db-all-list-format (get-list-format (car db-full)))
  )

(defun get-all-list-entries (lines)
  ;; input sequence of lines
  ;; each line is a string with comma separated values
  ;; return should be (list (list "1" ["id" "formula"]) 
  ;;                        (list "2" ["id" "formula"]))
  (let ((l1 (mapcar (lambda (arg) (split-string arg "|")) lines)))
	(mapcar
	 (lambda (arg)
	   (list
		(s-trim (car arg))
		;; nil
		(vconcat
		 (mapcar (lambda (a2) (s-trim a2)) arg)
		 )))
	 l1))
)

(defun get-list-entries (lines)
  ;; input sequence of lines
  ;; each line is a string with | separated values
  ;; return should be (list (list "1" ["id" "formula" ...]) 
  ;;                        (list "2" ["id" "formula" ...]))
  ;; Sorting out which indices to use first
  (let ((l1 (mapcar
			 (lambda (arg)
			   (mapcar
				(lambda (used-index)
				  (elt (split-string arg "|") used-index)) ase-db-columns-used-indices)) lines)))
	;; Making the correctly formatted list with only the indices to use
	(mapcar
	 (lambda (arg)
	   (list
		(s-trim (car arg))
		(vconcat
		 (mapcar (lambda (a2) (s-trim a2)) arg)
		 )))
	 l1))
)

(defun ase-db-set-all-keywords ()
  (setq ase-db-tmp-user-keywords 
		(mapcar
		 (lambda (arg) (s-trim arg))
		 (split-string (cadr (split-string (car (last db-full)) ":")) ",")))
  (setq ase-db-tmp-columns-not-shown ase-db-tmp-user-keywords)
  (setq ase-db-tmp-columns-to-show (append ase-db-columns ase-db-tmp-user-keywords))
  (let ((p ase-db-tmp-columns-to-show))
	(setq ase-db-tmp-columns-not-shown (append ase-db-default-columns ase-db-tmp-columns-not-shown))
	(while p
	  (setq ase-db-tmp-columns-not-shown (delete (car p) ase-db-tmp-columns-not-shown))
	  (setq p (cdr p)))
	)
)

(defvar ase-db-mode-map
  (let ((map (make-sparse-keymap))
		(menu-map (make-sparse-keymap "ase-db")))
	(set-keymap-parent map tabulated-list-mode-map)
    (define-key map "A" 'ase-gui-open)
    (define-key map "t" 'ase-db-write-trajectory)
	(define-key map "s" 'ase-db-list-sort)
	(define-key map "f" 'forward-thing)
	(define-key map "+" 'ase-db-add-column)
	(define-key map "-" 'ase-db-remove-column)
	(define-key map "S" 'ase-db-add-selection)
	(define-key map "D" 'ase-db-delete-row)
	(define-key map "g" 'ase-db-update-view)
	(define-key map "m" 'ase-db-mark)
	(define-key map "u" 'ase-db-unmark)
	(define-key map "U" 'ase-db-unmark-all)
	(define-key map "k" 'ase-db-add-key-value-pair)
	(define-key map (kbd "C-c C-c") 'ase-db-copy-row)
	(define-key map (kbd "C-c C-y") 'ase-db-paste-row)
	
	(define-key map [menu-bar ase-db]
	  (cons "ase-db" menu-map))
	(define-key menu-map [mq]
	  '(menu-item "Quit Window" quit-window
				  :help "Quit ase-db view"))
	(define-key menu-map [delete-row]
	  '(menu-item "Delete the current row" ase-db-delete-row
				  :help "Delete the current row in the database"))
	(define-key menu-map [paste-row]
	  '(menu-item "Paste row(s) from another ase-db buffer" ase-db-paste-row
				  :help "Paste a row (or rows if more are marked) from a different ase-db buffer."))
	(define-key menu-map [copy-row]
	  '(menu-item "Copy row(s) from this ase-db buffer" ase-db-copy-row
				  :help "Copy row (or rows if more are marked) with an eye to paste it/them in another buffer."))
	(define-key menu-map [remove-column]
	  '(menu-item "Remove column from view" ase-db-remove-column
				  :help "Remove a column from the view, default is the column at point."))
	(define-key menu-map [add-column]
	  '(menu-item "Add column to the view" ase-db-add-column
				  :help "Add a column to the view, type nothing or + means add all available columns"))
	(define-key menu-map [ase-gui]
	  '(menu-item "Open in ase-gui" ase-gui-open
				  :help "Open the selected row in ase-gui"))
	(define-key menu-map [write-traj]
	  '(menu-item "Write trajectory file" ase-db-write-trajectory
				  :help "Write the selected row as an atoms object to a trajectory file"))
	(define-key menu-map [add-key-value-pair]
	  '(menu-item "Add key-value-pair" ase-db-add-key-value-pair
				  :help "Add key-value-pair to current row or marked row(s)"))
	(define-key menu-map [move-down]
	  '(menu-item "Move Down" next-line
				  :help "Move down a row"))
	(define-key menu-map [unmark-all]
	  '(menu-item "Unmark all" ase-db-unmark-all
				  :help "Unmark all the selected rows"))
	(define-key menu-map [unmark]
	  '(menu-item "Unmark" ase-db-unmark
				  :help "Unmark the current row"))
	(define-key menu-map [mark]
	  '(menu-item "Mark" ase-db-mark
				  :help "Mark the current row"))
	(define-key menu-map [ms]
	  '(menu-item "Sort" ase-db-list-sort
				  :help "Sort by column at point"))
	(define-key menu-map [mS]
	  '(menu-item "Select" ase-db-add-selection
				  :help "Add a selection query"))
	(define-key menu-map [mU]
	  '(menu-item "Update" ase-db-update-view
				  :help "Update the view"))
	map)
  "Local keymap for `ase-db-mode' buffers."
  )

(define-derived-mode ase-db-mode tabulated-list-mode "ase-db"
  "Major mode for ase-db.
Letters do not insert themselves; instead, they are commands.
\\<ase-db-mode-map>
\\{ase-db-mode-map}"
  (ase-db-set-local-variables)
  (ase-db-get-used-column-indices)
  (setq tabulated-list-entries (get-list-entries (butlast (cdr db-full) 2)))
  (setq tabulated-list-format (ase-db-get-final-tabulated-list-format (car db-full)))
  ;; (ase-db-make-used-list-format)
  ;; (ase-db-make-list-format)
  ;; (setq tabulated-list-format (get-list-format (car db-full)))
  ;; (setq tabulated-list-entries (get-list-entries (butlast (cdr db-full) 2)))
  ;; (setq tabulated-list-entries ase-db-all-list-entries)
  ;; (setq tabulated-list-format new-list-format)
  ;; (setq tabulated-list-format ase-db-all-list-format)
  ;; (setq ase-db-list-format tabulated-list-format)
  (setq tabulated-list-padding 2)
  (setq tabulated-list-sort-key (cons "id" nil))
  (tabulated-list-init-header)
  (use-local-map ase-db-mode-map)
  (setq ase-db-marked-list (list))
)

(defun ase-db-get-used-column-indices ()
  (setq ase-db-columns-used-indices '())
  (let ((tmp-header-line (split-string (car db-full) "|"))
		(counter 0))
	(while tmp-header-line
	  (if (member (s-trim (car tmp-header-line)) ase-db-columns-to-show)
		  (setq ase-db-columns-used-indices (append ase-db-columns-used-indices (list counter)))
		  )
	  (setq tmp-header-line (cdr tmp-header-line))
	  (setq counter (1+ counter))
	  )
	)
  )

;; (defun ase-db-make-list-format ()
;;   (setq new-list-format '())
;;   (setq ase-db-columns-used-indices '())
;;   (let ((tmp-list-format ase-db-all-list-format)
;; 		(counter 0))
;; 	(while tmp-list-format
;; 	  (if (member (caar tmp-list-format) ase-db-columns-to-show)
;; 		  (progn
;; 			(setq new-list-format (append new-list-format (car tmp-list-format)))
;; 			(setq ase-db-columns-used-indices (append ase-db-columns-used-indices counter))
;; 			)
;; 		  )
;; 	  (setq tmp-list-format (cdr tmp-list-format))
;; 	  (setq counter (1+ counter)))
;; 	)
;;   )

(defun ase-db-make-used-list-format ()
  (setq tabulated-list-format (vconcat
							   (mapcar (lambda (arg)
										 (elt ase-db-all-list-format arg))
									   ase-db-columns-used-indices)))
  )

(defun ase-db-get-final-tabulated-list-format (firstline)
  (setq i -1)
  (setq j -1)
  (remove nil (vconcat
			   (mapcar 
				(lambda (arg)
				  (setq i (1+ i))
				  (if (member i ase-db-columns-used-indices)
					  (progn
						(setq j (1+ j))
						(list (s-trim arg)
							  (length arg)
							  (get-final-column-sorter-predicate (s-trim arg) j)
							  .
							  (:right-align t :pad-right ase-db-column-padding)))))
				(split-string firstline "|"))))
  )

(defun get-list-format (firstline)
  (setq i -1)
  (vconcat (mapcar 
			(lambda (arg)
			  (setq i (1+ i))
			  (list (s-trim arg) (length arg) (get-column-sorter-predicate (s-trim arg) i) . (:right-align t :pad-right ase-db-column-padding)))
			(split-string firstline "|")))
)

(defun ase-db-is-number (arg)
  (if (or (string-float-p arg) (string-integer-p arg)) 't 'nil)
  )

(defun get-column-sorter-predicate (name counter)
  "Return the predicate for sorting the column by NAME.
COUNTER is the index at which NAME occurs in the first line.
Basically it is the function < as predicate for numbers or 't for strings."
  (let ((c counter))  ;; changed from lexical-let
	(if (ase-db-is-number (elt (car (cdar ase-db-all-list-entries)) c))
		#'(lambda (A B)
			(< (string-to-number (elt (car (cdr A)) c))
			   (string-to-number (elt (car (cdr B)) c))))
	  't)
	)
  )

(defun get-final-column-sorter-predicate (name counter)
  "Return the predicate for sorting the column by NAME.
COUNTER is the index at which NAME occurs in the first line.
The difference between this and get-column-sorter-predicate is that this
function use 'tabulated-list-entries' in which not all columns are present.
Basically it is the function < as predicate for numbers or 't for strings."
  (let ((c counter))  ;; changed from lexical-let
	(if (ase-db-is-number (elt (car (cdar tabulated-list-entries)) c))
		#'(lambda (A B)
			(< (string-to-number (elt (car (cdr A)) c))
			   (string-to-number (elt (car (cdr B)) c))))
	  't)
	)
  )


(defun do-ase-db (db-name selection columns)
  "Do the actual call to ase-db."
  (let ((command (format "ase db %s '%s' -c %s -L 0" db-name selection columns)))
	(s-trim-right (shell-command-to-string command)))
  ;; (let ((command (format "ase db %s '%s' -c %s -L 0" db-name selection (mapconcat 'identity columns ","))))
  ;; 	(s-trim-right (shell-command-to-string command)))
  )

(setq-default ase-db-columns (list "id" "age" "formula" "energy" "fmax" "volume" ))
;; (setq ase-db-columns-to-show (list "id" "age" "formula" "energy" "fmax" "volume" ))
;; (setq ase-db-columns-not-shown (list))
(setq-default ase-db-selection "")
(setq ase-db-column-padding 2)
(setq ase-db-default-columns (list "id" "age" "user" "formula" "calculator" "energy" "fmax" "pbc" "volume" "charge" "mass" "magmom"))

(defun print-location ()
  (interactive)
  (message ase-db-location)
  )

(defun print-selection ()
  (interactive)
  (message ase-db-selection)
  )

(defun ase-db-list-sort ()
  (interactive)
  (tabulated-list-sort)
  (save-excursion
  	(goto-char (point-min))
	(while (not (eobp))
	  (if (member (ase-db-get-row-id) ase-db-marked-list)
		  (tabulated-list-put-tag "*")
		)	
	  (ase-db-next-line 1)
	  )
  	)
  )

(defun ase-db-mark ()
  (interactive)
  (let ((current-row-id (ase-db-get-row-id)))
	(if (member current-row-id ase-db-marked-list)
		(message "Already marked")
	  (progn
		(setq ase-db-marked-list
			  (append ase-db-marked-list (list current-row-id)))
		(tabulated-list-put-tag "*" t)
		)
	  )
	)
  )

(defun ase-db-get-row-id ()
  "Return the id of the current row.
Needs this function as the mark can mess up 'tabulated-list-get-id.'"
  (save-excursion
	(beginning-of-line)
	(forward-char)
	(tabulated-list-get-id)
	)
  )

(defun ase-db-unmark ()
  (interactive)
  (setq ase-db-marked-list
		(remove (ase-db-get-row-id) ase-db-marked-list))
  (tabulated-list-put-tag " " t)
  )

(defun ase-db-unmark-all ()
  (interactive)
  (setq ase-db-marked-list (list))
  (save-excursion
	(goto-char (point-min))
	(while (not (eobp))
	  (tabulated-list-put-tag " " t)
	  )
	)
  )

(defun ase-db-print-marked ()
  (interactive)
  (message ase-db-marked-list)
  )

(defun ase-db-add-selection (sel)
  ;; (interactive "sSelection: ")
  (interactive (list (read-string "Selection: " (ase-db-get-selection-string))))
  ;; Is it confusing that the user can select using other things than what is being suggested?
  ;; (interactive (let ((candidates (append ase-db-default-columns ase-db-user-keywords)))
  ;; 				 (list (ido-completing-read "Selection: " candidates))))
  ;; (interactive (let ((candidates (append ase-db-default-columns ase-db-user-keywords)))
  ;; 				 (list (completing-read "Selection: " candidates))))
  (if (string-suffix-p "," sel)
	  (setq sel (substring sel 0 -1)))
  (setq-local ase-db-old-selection ase-db-selection)
  (setq-local ase-db-selection sel)
  (ase-db-full-revert)
  )

(defun ase-db-get-selection-string ()
  ;; (identity ase-db-selection)
  (if (string= ase-db-selection "")
  	  (identity ase-db-selection)
  	(concat ase-db-selection ","))
  )

(defun ase-db-add-column (c)
  ;; (interactive (list (read-string "Add column: " nil 'ase-db-columns-not-shown "+")))
  ;; what about completing-read
  ;; (interactive (let ((candidates ase-db-columns-not-shown))
  ;; 				 (list (ido-completing-read "Add column: " candidates))))
  (interactive (let ((candidates ase-db-columns-not-shown))
				 (list (completing-read "Add column: " candidates))))
  (if (string= c "+")
	  (setq-local ase-db-columns-to-show (append ase-db-default-columns ase-db-user-keywords))
	(setq-local ase-db-columns-to-show (append ase-db-columns-to-show (list c))))
  (ase-db-revert)
  )

(defun ase-db-get-column-name ()
  (if (equal 0 (current-column))
	  (car (elt tabulated-list-format 0))
	(progn (setq ase-db-counter 0)
		   (setq ase-db-i 0)
		   (setq ase-db-tlf (elt tabulated-list-format 0))
		   (while (< ase-db-counter (current-column))
			 (setq ase-db-counter
				   (+ ase-db-column-padding
					  (+ ase-db-counter (cadr (elt tabulated-list-format ase-db-i)))))
			 (setq ase-db-i (1+ ase-db-i))
			 )
		   (car (elt tabulated-list-format (1- ase-db-i)))
		   )
	)
  )

(defun ase-db-remove-column ()
  "Remove the column at point."
  (interactive)
  (let ((curcol (ase-db-get-column-name)))
	(setq-local ase-db-columns-to-show (remove curcol ase-db-columns-to-show))
	(setq-local ase-db-columns-not-shown (append ase-db-columns-not-shown (list curcol)))
	)
  ;; (message (format "Entry: %s" ase-db-columns-to-show))
  (ase-db-revert)
  )

(defun ase-db-remove-column-old (c)
  (interactive (list (read-string "Remove column: " nil 'ase-db-columns "+")))
  (if (string= c "+")
	  (let ((curcol (ase-db-copy-column)))
		(message (format "Current column: " curcol))
		(setq-local ase-db-columns (remove curcol ase-db-columns)))
	(setq-local ase-db-columns (remove c ase-db-columns)))
  (ase-db-revert)
  )

(defun ase-db-set-local-variables ()
  "Make some variables that are likely to be altered local to this buffer."
  (setq-local ase-db-columns-to-show ase-db-tmp-columns-to-show)
  (setq-local ase-db-columns-not-shown ase-db-tmp-columns-not-shown)
  (setq-local ase-db-user-keywords ase-db-tmp-user-keywords)
  (make-local-variable 'ase-db-columns)
  (make-local-variable 'ase-db-selection)
  (make-local-variable 'ase-db-location)
  ;; (make-local-variable 'db-full)
  (make-local-variable 'ase-db-all-list-entries)
  (make-local-variable 'ase-db-all-list-format)
  (make-local-variable 'ase-db-marked-list)
  )

(defun ase-db-revert ()
  "Revert view but save a few variables first.
Reset the 'ase-db-marked-list' variable."
  (setq ase-db-marked-list (list))
  (setq ase-db-tmp-columns ase-db-columns)
  (setq ase-db-tmp-selection ase-db-selection)
  (setq ase-db-tmp-columns-not-shown ase-db-columns-not-shown)
  (setq ase-db-tmp-columns-to-show ase-db-columns-to-show)
  (setq ase-db-tmp-location ase-db-location)
  (tabulated-list-revert)
  (setq ase-db-columns ase-db-tmp-columns)
  (setq ase-db-selection ase-db-tmp-selection)
  (setq ase-db-columns-not-shown ase-db-tmp-columns-not-shown)
  (setq ase-db-columns-to-show ase-db-tmp-columns-to-show)
  (setq ase-db-location ase-db-tmp-location)
  )

(defun ase-db-full-revert ()
  "Repopulate db-full and the revert the view."
  (setq db-full (split-string (do-ase-db ase-db-location ase-db-selection "++") "\n"))
  (if (string= (cadr db-full) "Rows: 0")
	  (progn
		(message "OBS: This selection returns 0 rows!")
		(setq-local ase-db-selection ase-db-old-selection)
		(setq db-full (split-string (do-ase-db ase-db-location ase-db-selection "++") "\n"))
		)
	  )
  (ase-db-revert)
  )

;;Shortcut for ase-db on the currently marked file in dired
(require 'dired)
(add-hook 'dired-mode-hook
 (lambda ()
  (define-key dired-mode-map (kbd "C-c C-a")
    (lambda () (interactive) (ase-db (dired-get-filename))))
 ))

(defun ase-db-update-view ()
  (interactive)
  (let ((ase-db-temp-point (point)))
	(ase-db-full-revert)
	(goto-char ase-db-temp-point)
	)
  )

(defun ase-db-add-key-value-pair (kvp)
  "Add a key-value-pair (KVP) to the current or selected row(s)."
  (interactive (list (read-string "Add key value pair: ")))
  ;; Add keys to ase-db-columns-to-show so the new key value pair will be shown right away
  (let ((ss (split-string kvp ",")))
  	(while ss
  	  (setq ase-db-columns-to-show
			(append ase-db-columns-to-show (list (car (split-string (car ss) "=")))))
  	  (setq ss (cdr ss))
  	  )
  	)
  (if ase-db-marked-list
	  (progn 
		(setq ase-db-operate-list ase-db-marked-list)
		)
	(setq ase-db-operate-list (list (ase-db-get-row-id)))
	)
  (let ((ase-db-temp-point (point)))
	(while ase-db-operate-list
	  (setq id-to-operate-on (car ase-db-operate-list))
	  (setq ase-db-operate-list (cdr ase-db-operate-list))
	  (call-process-shell-command (format "ase db %s id=%s -k %s" ase-db-location id-to-operate-on kvp))
	  )
	(ase-db-full-revert)
	)
  )

(defun ase-db-delete-row ()
  "Delete the current row or marked rows from the database."
  (interactive)
  (if ase-db-marked-list
	  ;; 'ase-db-marked-list' is not empty, delete all marked
	  (progn
		(setq ase-db-delete-string (mapconcat 'identity ase-db-marked-list ","))
		)
	;; 'ase-db-marked-list' is empty, delete current row
	(progn
	  (setq ase-db-delete-string (ase-db-get-row-id))
	  )
	)
  (let ((ase-db-temp-point (point)))
	(if (y-or-n-p (format "Delete row(s) with id(s)=%s?" ase-db-delete-string))
		(progn
		  (ase-db-do-delete-python ase-db-delete-string)
		  (ase-db-full-revert)
		  (message (format "Deleted id(s)=%s" ase-db-delete-string))
		  )
	  )
	(goto-char ase-db-temp-point)
	)
  )

(defun ase-db-do-delete-python (delete-string)
  (call-process-shell-command (format "python -c \"from ase.db import connect; db = connect('%s'); db.delete([%s])\"" ase-db-location delete-string))
  )

(defun ase-db-copy-row ()
  (interactive)
  (if ase-db-marked-list
	  (progn
		(setq ase-db-copy-string (mapconcat 'identity ase-db-marked-list ","))
		(setq ase-db-copy-list ase-db-marked-list)
		)
	(progn
	  (setq ase-db-copy-string (ase-db-get-row-id))
	  (setq ase-db-copy-list (list (ase-db-get-row-id)))
	  )
	)
  (setq ase-db-copy-location ase-db-location)
  (message (format "Copied id(s)=%s from %s" ase-db-copy-string ase-db-copy-location))
  )

(defun ase-db-paste-row ()
  (interactive)
  (if (or (not (boundp 'ase-db-copy-list)) (not ase-db-copy-list))
	  (message "No rows are copied!")
	(if (eq ase-db-location ase-db-copy-location)
		(message "Copy to same db is not allowed!")
	  (progn
		(while ase-db-copy-list
		  (setq id-to-copy (car ase-db-copy-list))
		  (setq ase-db-copy-list (cdr ase-db-copy-list))
		  (ase-db-do-copy id-to-copy)
		  )
		(ase-db-full-revert)
		(message (format "Pasted id(s)=%s from %s" ase-db-copy-string ase-db-copy-location))
		)
		)
	  )
  )

(defun ase-db-do-copy (id-to-copy)
  (call-process-shell-command (format "ase db %s id=%s -i %s" ase-db-copy-location id-to-copy ase-db-location))
  )

(defun ase-gui-open (arg)
  "Open ase-gui with the row at point.  ARG is nothing."
  (interactive "P")
  (start-process-shell-command "ase gui" (buffer-name) (format "ase gui %s@%s" ase-db-location (ase-db-get-row-id)))
  )

(defun ase-db-write-trajectory (fname)
  "Write a file of the row at point"
  (interactive (list (read-string "File name: " (cons ".traj" 1))))
  (start-process-shell-command "write-trajectory" (buffer-name) (format "ase gui %s@%s -o %s" ase-db-location (ase-db-get-row-id) fname))
  )

(defun ase-db-copy-column ()
  "Copies the column at the cursor."
  ;; (interactive "P")
  (save-excursion
	(goto-char (current-column))
	(let ((beg (get-point 'backward-word 1))
		  (end (get-point 'forward-word 1)))
	  (buffer-substring beg end)))
  )

(defun ase-db-copy-id ()
  "Copies the id of the row at the cursor."
  ;; (interactive "P")
  (save-excursion
	(beginning-of-line)
	(let ((beg (point))
		  (end (get-point 'forward-word arg)))
	  (s-trim (buffer-substring beg end))))
  )

(defun ase-db-next-line (arg)
  "Move to next line.
ARG is the amount of lines to move."
  (interactive "p")
  (line-move arg t))

(defun ase-db-previous-line (arg)
  "Move up line.
ARG is how many lines to move."
  (interactive "p")
  (ase-db-next-line (- (or arg 1))))

;; Remaining functions are cut from various sources on the web
(defun get-point (symbol &optional arg)
  "Get the point."
  (funcall symbol arg)
  (point)
  )

(defun s-trim-left (s)
  "Remove whitespace at the beginning of S."
  (if (string-match "\\`[ \t\n\r]+" s)
      (replace-match "" t t s)
    s))

(defun s-trim-right (s)
  "Remove whitespace at the end of S."
  (if (string-match "[ \t\n\r]+\\'" s)
      (replace-match "" t t s)
    s))

(defun s-trim (s)
  "Remove whitespace at the beginning and end of S."
  (s-trim-left (s-trim-right s)))

(defun string-integer-p (string)
   (if (string-match "\\`[-+]?[0-9]+\\'" string)
       t
     nil))

(defun string-float-p (string)
   (if (string-match "\\`[-+]?[0-9]+\\.[0-9]*\\'" string)
       t
     nil))

;; (defun id-predicate (A B)
;;   (< (string-to-number (elt (car (cdr A)) 0)) (string-to-number (elt (car (cdr B)) 0)))
;;   )

;; (defun energy-predicate (A B)
;;   (< (string-to-number (elt (car (cdr A)) 2)) (string-to-number (elt (car (cdr B)) 2)))
;;   )

;; (defun fmax-predicate (A B)
;;   (< (string-to-number (elt (car (cdr A)) 3)) (string-to-number (elt (car (cdr B)) 3)))
;;   )

(provide 'ase-db)
;;; ase-db.el ends here

