;;; ase-db.el --- Use ase db in emacs -*- lexical-binding: t -*-

;; Copyright (C) 2015 Steen Lysgaard

;; Author: Steen Lysgaard <st.lysgaard@gmail.com>
;; URL: Nowhere
;; Keywords: ase-db, Atomic Simulation Environment

;;; Commentary:
;;
;; Start it with M-x ase-db
;; then you will get prompted for the location of the db file.
;;
;; Enjoy!

;;; Code:

(require 'tabulated-list)

;; tablist extends tabulated-list, mostly with marks and other useful
;; functions
(require 'tablist)

;; we need to trim whitspace from strings
(require 'subr-x)

;; find entries are defined in ase-db-entry.el
(require 'ase-db-entry)

;; Define special variables
(defvar ase-db-location)
(defvar ase-db-selection)
(defvar ase-db-open-dbs (list))
;; When was the db last read, a buffer local variable. It is
;; initialized to January 1. 2000, i.e. we can't read dbs older than
;; that
(defvar *ase-db-time-last-read* (list 14445 13680 0 0))
;; Turn on debug messages if ase-db-debug is non-nil.
(defvar ase-db-debug nil)
;; Remember the original widths obtained from python in an alist.
(defvar ase-db-original-col-widths (list))

;; Define variables that can be changed by the user
(defcustom ase-db-column-padding 2
  "The number of additional padding spaces to the right of a column."
  :type 'number
  )

;; Get the Python file name in order to be able to load the class in the
;; interactive python process
(defconst ase-db-python-source-folder (file-name-directory load-file-name))

(defun ase-db (address &optional sel)
  "The main function.

ADDRESS is the location of the db file.
SEL is the selection."
  (interactive "fdb file:\nsSelection (optional):\nsOptions (optional): ")
  ;; Find the absolute location of the db - maybe this needs to allow
  ;; server dbs as well. ase-db-loc is temporary as we can't make it
  ;; buffer local before we switch to the appropriate buffer
  (setq ase-db-loc (file-truename address))
  (if (> (count-windows) 1)
      (switch-to-buffer-other-window (concat "ase db " ase-db-loc))
    (switch-to-buffer (concat "ase db " ase-db-loc))
      )

  (if ase-db-debug
      (progn
        (message (concat "Selection (bracketed): [" sel "]"))
        (message "ase-db-selection: " )
        (if (boundp 'ase-db-selection)
            (message ase-db-selection)
          (message "ase-db-selection is void / unbound"))
        )
      )
  
  ;; Test if the db is already open or the selection SEL is different
  ;; than ASE-DB-SELECTION
  (if (or (not (member ase-db-loc ase-db-open-dbs))
          (and (not (string-equal sel "")) (boundp 'ase-db-selection)
               (not (string-equal sel ase-db-selection))))
      (progn
        ;; Add a space before each entry or else making a mark will move the
        ;; entire table
        (setq tabulated-list-padding 1)
        
        (ase-db-mode)
        
        ;; Set the location of the database, this should be a buffer local
        ;; variable as there may be more buffers with different databases
        (setq-local ase-db-location ase-db-loc)
        
        ;; Set the selection
        (setq-local ase-db-selection sel)
        
        ;; Initialize a Python process if one isn't open already
        (ase-db-init-python)

        ;; Connect to the database
        (ase-db-connect-to-database ase-db-location)
        (ase-db-test-connection)
        
        ;; Remember that we have opened this db
        (add-to-list 'ase-db-open-dbs ase-db-location)

        ;; Specify the key that is sorted by initially
        (setq tabulated-list-sort-key (cons "id" nil))
        
        ;; Force read data since nothing is there at the moment
        (ase-db-read t t)
        
        )
    ;; else: i.e. the db is already open
    (progn
      ;; hydra is not started when reopening the buffer, so we do it manually
      (hydra-ase-db/body)

      ;; Read data if the current information is not up-to-date
      (ase-db-read)
      )
      )

  )

(defvar ase-db-mode-map
  (let ((kmap (copy-keymap tablist-mode-map)))
    (set-keymap-parent kmap tabulated-list-mode-map)
    ;; Navigation
    ;; Page up and down

    ;; ;; Marking
    ;; Mark all
    (define-key kmap "M" 'ase-db-mark-filter)
    ;; (define-key kmap "m" 'tablist-mark-forward)
    ;; (define-key kmap "u" 'tablist-unmark-forward)
    ;; (define-key kmap "U" 'tablist-unmark-all-marks)
    ;; (define-key kmap "d" 'tablist-flag-forward)

    ;; ;; Visual
    (define-key kmap "F" 'ase-db-push-filter)
    (define-key kmap "-" 'ase-db-remove-column)
    (define-key kmap "+" 'ase-db-add-column)
    
    ;; Editing
    (define-key kmap "a" 'ase-db-add-key-value-pair)
    
    ;; Misc
    (define-key kmap "v" 'ase-db-ase-gui-view)
    (define-key kmap "V" 'ase-db-ase-gui-view-marked)
    (define-key kmap "c" 'ase-db-copy-into-other-db)
    (define-key kmap "S" 'ase-db-export-to-traj)
    (define-key kmap "*" 'ase-db-special-mark)
    
    ;; Export
    (define-key kmap "e" 'ase-db-export-to-csv)
    
    ;; Help
    (define-key kmap  "." 'hydra-ase-db/body)
    kmap)
  )

(defhydra hydra-ase-db (:color pink
                        :hint nil)
  "
^Mark^                ^Delete^             ^Actions^                    ^Manipulation^                ^Filter^
^-^-------------------^-^------------------^-^--------------------------^-^---------------------------^-^------------
_m_ark row            _x_: delete flagged  _v_iew row                   _+_: Add column to view       _F_ilter rows
_u_nmark row          flag for _d_eletion  _V_iew marked rows           _-_: Remove column from view  _E_dit filter
_M_ark more rows      _D_elete marked      _a_dd key value pair / data  _<_: Shrink column size       _C_lear filters
_U_nmark all rows     ^^                   _e_xport to csv              _>_: Enlarge column size
_t_oggle marks        ^^                   _c_opy into other db         _s_ort by current column
_*_: Mark with python query ^^             _S_ave marked to trajectory  S-←/→ (↑/↓): Move column (row)

"
  ("m" tablist-mark-forward)
  ("M" ase-db-mark-filter)
  ("*" ase-db-special-mark)
  ("v" ase-db-ase-gui-view)
  ("V" ase-db-ase-gui-view-marked)
  ("a" hydra-ase-db-add-stuff/body :color blue)
  ("+" ase-db-add-column)
  ("-" ase-db-remove-column)
  ("<" tablist-shrink-column)
  (">" tablist-enlarge-column)
  ("s" tablist-sort)
  ("t" tablist-toggle-marks)
  ("d" tablist-flag-forward)
  ("x" tablist-do-flagged-delete)
  ("D" tablist-do-delete)
  ("U" tablist-unmark-all-marks)
  ("u" tablist-unmark-forward)
  ("F" ase-db-push-filter)
  ("E" tablist-edit-filter)
  ("C" tablist-clear-filter)
  ("c" ase-db-copy-into-other-db)
  ("S" ase-db-export-to-traj)
  ("e" ase-db-export-to-csv)
  ("g" ase-db-update-data)
  ("P" ase-db-run-python-function)
  ("O" ase-db-adjust-widths)
  ("S-<left>" ase-db-move-column-left)
  ("S-<right>" ase-db-move-column-right)
  ("S-<up>" ase-db-move-row-up)
  ("S-<down>" ase-db-move-row-down)
  
  ("<return>" tablist-find-entry)
  
  ("q" ase-db-kill-emacs "Quit ase-db-tui" :color blue)
  ("." nil "Toggle help" :color blue)
  )

(defhydra hydra-ase-db-add-stuff (:hint nil
                                  :color pink)
  "
Add stuff
---------
_k_ey value pair                _d_ata
"
  ("k" ase-db-add-key-value-pair)
  ("d" ase-db-add-data)
  ("q" hydra-ase-db/body "go back" :exit t)
  )

(defun ase-db-process-command-line-args ()
  "Process the command line arguments and call the function `ase-db'.

Only used when running single emacs instance."
  (let ((ase-db-options (list))
        (ase-db-selection nil)
        ;; the filename should always come after emacs i.e. should be
        ;; number two in the list where the counting starts from 0
        (ase-db-filename (nth 1 command-line-args)))
    (dolist (var (reverse command-line-args))
      (if (catch 'binop
            (mapc
             (lambda (x)
               (when (string-match-p x var)
                 (throw 'binop t)))
             (list "=" "<" ">" "<=" ">="))
            nil)
          (setq ase-db-selection var)
        )
      
      )
    ;; (ase-db ase-db-filename ase-db-selection)
    (define-key ase-db-mode-map "q" 'kill-emacs)
    (setq command-line-args (list "emacs"))
    )
  
  )

(defun ase-db-exit-with-output ()
  (let ((proc (frame-parameter nil 'client)))
    (server-delete-client proc)
    )
  ;; (kill-emacs " command cat /tmp/ase-db-tui-output; rm -f /tmp/ase-db-tui-output")
  )

(defun ase-db-kill-emacs ()
  "Exit with printing the current view to the terminal."
  (interactive)
  ;; Save the keys file to remember the chosen keys
  (ase-db-call-python-function-no-output "save_keys()")
  ;; Check to see if the file already exists
  (write-region (concat header-line-format "\n") nil "/tmp/ase-db-tui-output")
  ;; (print (concat header-line-format "\n") #'external-debugging-output)
  (append-to-file (window-start) (window-end) "/tmp/ase-db-tui-output")
  ;; (print (window-start) (window-end) "/dev/stdout")
  ;; If it is annoying that this command goes into the terminal
  ;; history please add ignorespace to the HISTCONTROL environment
  ;; variable, e.g. export HISTCONTROL=ignorespace
  (ase-db-exit-with-output)
  )

(define-derived-mode ase-db-mode tablist-mode "ase-db"
  "Major mode for ase-db.
Letters do not insert themselves; instead, they are commands.
\\<ase-db-mode-map>
\\{ase-db-mode-map}"
  
  (use-local-map ase-db-mode-map)
  (setq-local tablist-operations-function 'ase-db-operations)
  (tablist-move-to-major-column)
  (setq tabulated-list-printer 'ase-db-entry-printer)
  (hydra-ase-db/body)
)

(defun ase-db-set-tabulated-list-format ()
    "Set the header of the table
The documentation of the tabulated-list-format says the following: This buffer-local variable specifies the format of the Tabulated List data. Its value should be a vector. Each element of the vector represents a data column, and should be a list (name width sort),

The fact that width (and sort) is required makes it necessary to have knowledge of the content of the column (not just the name) before setting the header. Should be sorted out in Python"
    
    (ase-db-call-python-function "get_header()")
    ;; Reset the column widths as they will be set in the function
    ;; ase-db-split-header-string
    (setq ase-db-original-col-widths (list))
    (setq tabulated-list-format
          (vconcat (mapcar 'ase-db-split-header-string
                           (butlast (split-string ase-db-output "\n"))
                           )
                   ))
    )

(defun ase-db-get-predicate (python_predicate_string index_in_header)
  (let ((c index_in_header))
    (cond ((string= python_predicate_string "numbers")
           #'(lambda (A B)
               (< (string-to-number (elt (cadr A) c))
                  (string-to-number (elt (cadr B) c)))
               ))
          ((string= python_predicate_string "age-sort")
           #'(lambda (A B)
               (< (ase-db-get-ctime-from-age-string (elt (cadr A) c))
                  (ase-db-get-ctime-from-age-string (elt (cadr B) c)))
               ))
          (t 't))
    ;; (if (string= python_predicate_string "numbers")
    ;;     #'(lambda (A B)
    ;;         (< (string-to-number (elt (cadr A) c))
    ;;            (string-to-number (elt (cadr B) c)))
    ;;         )
    ;;   't
    ;;   )
    )
  )

(defun ase-db-get-ctime-from-age-string (age-string)
  "Removes the -AGE- part from the AGE-STRING and returns the ctime as a float."
  (string-to-number (substring age-string 5))
  )

(defun ase-db-split-header-string (in_string)
  (let* ((ch (split-string in_string ","))
         (col-width (string-to-number (car (cdr ch)))))
    (add-to-list 'ase-db-original-col-widths (cons (car ch) col-width))
    (list
     (car ch)  ;; Name of the column
     col-width  ;; Width (an integer)
     (ase-db-get-predicate (elt ch 2) (string-to-number (car (last ch))))  ;; predicate function
     .
     (:right-align t :pad-right ase-db-column-padding)
     )
       )
  )

(defun ase-db-set-tabulated-list-entries ()
  "Sets the entries of the table
The documentation of the tabulated-list-entries says the following: This buffer-local variable specifies the entries displayed in the Tabulated List buffer. Its value should be either a list, or a function.

If the value is a list, each list element corresponds to one entry, and should have the form (id contents), where

- id is either nil, or a Lisp object that identifies the entry. If the latter, the cursor stays on the same entry when re-sorting entries. Comparison is done with equal.
- contents is a vector with the same number of elements as tabulated-list-format. Each vector element is either a string, which is inserted into the buffer as-is, or a list (label . properties), which means to insert a text button by calling insert-text-button with label and properties as arguments (see Making Buttons).

There should be no newlines in any of these strings.

Otherwise, the value should be a function which returns a list of the above form when called with no arguments.

"
  ;; I.e. the return should be (list (list "1" ["1" "C2H4" ...])
  ;;                                 (list "2" ["2" "H2O" ...]))

  (ase-db-call-python-function "get_all_rows()")
  
  (setq tabulated-list-entries
        (mapcar 'ase-db-split-entry-string
                (butlast (split-string ase-db-output "\n")))
        )
  )

(defun ase-db-split-entry-string (in_string)
  "Getting a string with 1,3,a,K,4 and returning (list 1 [1 3 a K 4])

Everything as strings.
"
  (let ((entries (split-string in_string ",")))
    (list
     (car entries)
     ;; nil
     (vconcat entries)
     )
    )
  )

(defun ase-db-entry-printer (id cols)
  (tabulated-list-print-entry id (vconcat (mapcar 'ase-db-format-entry cols)))
  )

(defun ase-db-format-entry (entry)
  (if (string-match "\\`[\-0-9]*[.0-9][0-9]*\\'" entry)
      ;; An integer or float
      (let ((number-entry (string-to-number entry)))
        (if (integerp number-entry)
          ;; integer
          entry
        ;; float, adding # allows the alternate specification where
        ;; the decimal point is always shown and so is trailing zeros
        ;; for "normal" numbers we regular %.2f for very large or very
        ;; small number we use %#.3g
        (if (or (> (abs number-entry) 1e6) (< (abs number-entry) 1e-4))
            (format "%#.3g" number-entry)
          (format "%.2f" number-entry)
            )))
    ;; Something with letters in, we assume it is a string
    (if (and (> (length entry) 5) (string= (substring entry 0 5) "-AGE-"))
        ;; It is the age of the row, we have to calculate the string
        (ase-db-get-age-from-ctime (ase-db-get-ctime-from-age-string entry))
      ;; Else we return the entry as is
      entry
        )
    )
  )

(defun ase-db-get-age-from-ctime (ctime)
  "Calculate a short string signifying the time from creation time (CTIME) until now.

The resulting string should have at least 5 unit time otherwise
the next time unit is used. E.g. 4m will instead be 240s, this
mirrors the functionality in ASE."
  (let* ((YEAR 31557600)
         (T2000 946681200)
         (seconds-time (list
                        (cons "y" YEAR)
                        (cons "M" 2629800)
                        (cons "w" 604800)
                        (cons "d" 86400)
                        (cons "h" 3600)
                        (cons "m" 60)
                        (cons "s" 1)))
        (res-int 0)
        (diff (- (- (float-time) T2000) (* ctime YEAR))))
    (while (and seconds-time (< res-int 5))
      (setq res-int (floor (/ diff (cdar seconds-time)))
            ;; setting the time diff as mod only required when
            ;; returning exact time diff
            ;; diff (mod diff (cdar seconds-time))
            res (concat (number-to-string res-int) (caar seconds-time))
            seconds-time (cdr seconds-time)))
    res
    )
  )

(defun ase-db-adjust-widths ()
  "Adjust the width of each column so that, if possible, each column name is shown.

1. Get the width of all columns with padding.
2. If the width is smaller than (window-width) then fine show everything, else:
3. Lower the width of the widest columns in turn to fit within the window, however:
3a. The width of a column should not be smaller than the column name size
4. If all columns still cannot fit, then so be it, the user will have to remove or move some columns."

  (interactive)
  (let* ((tot-width-and-widest-col (ase-db-get-width-and-widest-col))
         (tot-width (car tot-width-and-widest-col))
         (widest-col (cadr tot-width-and-widest-col))
         (second-widest-width (car (nthcdr 2 tot-width-and-widest-col)))
         (swapped nil)
         (reiterate nil))
    (when (= widest-col (1- (length tabulated-list-format)))
      ;; tablist-shrink-column and tablist-enlarge-column can't resize
      ;; the last column so we swap the last and second last. When the
      ;; column is situated at the last column it will not cut off the
      ;; widest value, but it will move the column name.
      (ase-db-swap-columns widest-col (1- widest-col))
      (setq swapped t)
      (setq widest-col (1- widest-col))
      )

    (if (> tot-width (window-width))
        ;; Content is wider than the window -> decrease the width
        (let ((header-element-list
               (aref tabulated-list-format widest-col)))

          ;; What is greater excess width or advantage over second widest?
          (if (> (- tot-width (window-width))
                 (- (elt header-element-list 1) second-widest-width))
              ;; excess width is greatest i.e. we shrink with the advantage over
              ;; second (and a little extra) and reiterate
              (progn
                (tablist-shrink-column widest-col (+ 2 (- (elt header-element-list 1)
                                                          second-widest-width)))
                (setq reiterate t))
            
            ;; advantage over second is greatest i.e. we take all
            ;; excess from widest column
            (tablist-shrink-column widest-col (- tot-width (window-width)))
            ))
      
      ;; Content is narrower than the window -> increase the widths
      ;; closer to original values
      ;; Find how much extra space can be used, add one to each column lacking and reiterate
      (let ((lacking-width (- (window-width) tot-width))
            (added-width 0)
            (column-i 0)
            (something-added-last-iteration t))
        (while (and (<= added-width lacking-width)
                    something-added-last-iteration)
          ;; variable to track whether any width was added last
          ;; iteration, if nothing was added everything is at original
          ;; width and we need to terminate the loop
          (setq something-added-last-iteration nil)
          (while (and (<= added-width lacking-width)
                      (< column-i (1- (length tabulated-list-format))))
            (let ((header-element-list (aref tabulated-list-format column-i)))
              ;; if actual width is smaller than original width then add
              ;; one to width
              (if (< (elt header-element-list 1)
                     (cdr (assoc (car header-element-list) ase-db-original-col-widths)))
                  (progn
                    (tablist-enlarge-column column-i 1)
                    (setq added-width (1+ added-width))
                    (setq something-added-last-iteration t))))
            (setq column-i (1+ column-i))
            )
          (setq column-i 0)
          )
        )
      )
    
    (if swapped
        ;; Swap last and second last columns back
        (ase-db-swap-columns widest-col (1+ widest-col))
      )
    (if reiterate
        (ase-db-adjust-widths))
    )
  )

(defun ase-db-get-width-and-widest-col ()
  "Return a list containing the total width, the index of the widest column and the second widest column width."
  (let ((cwidth 0)
        (widest-col-width 0)
        (widest-col-idx 0)
        (col-counter 0)
        (second-widest-width 0))
     (mapc
       (lambda (chead)
         (let ((this-col-width (elt chead 1)))
           (setq cwidth (+ cwidth this-col-width ase-db-column-padding))
           ;; if this column is the widest save it, unless it can't be
           ;; narrowed (i.e. when width already matches column name length)
           (if (> this-col-width (length (car chead)))
               (if (> this-col-width widest-col-width)
                   (progn
                     (setq second-widest-width widest-col-width)
                     (setq widest-col-width this-col-width)
                     (setq widest-col-idx col-counter)
                     )
                 ;; narrower than widest, but it could still be wider than
                 ;; second widest
                 (if (> this-col-width second-widest-width)
                     (setq second-widest-width this-col-width))
                 ) 
               )
           )
         (setq col-counter (1+ col-counter))
         )
      tabulated-list-format)
     (list cwidth widest-col-idx second-widest-width)
     )
  )

(defun ase-db-move-row-up ()
  (interactive)
  (let ((currow (line-number-at-pos)))
    (unless (equal currow 1)
      (ase-db-swap-rows (1- currow) (- currow 2))))
  )

(defun ase-db-move-row-down ()
  (interactive)
  (let ((currow (line-number-at-pos)))
    (unless (equal currow (length tabulated-list-entries))
      (ase-db-swap-rows (1- currow) currow)))
  )

(defun ase-db-swap-rows (row1 row2)
  (cl-rotatef (nth row1 tabulated-list-entries) (nth row2 tabulated-list-entries))
  (setq tabulated-list-sort-key nil)
  (tablist-save-marks
   (tabulated-list-print t))
  )

(defun ase-db-move-column-left ()
  (interactive)
  (let ((curcol (ase-db-get-current-column)))
    (ase-db-swap-columns curcol (1- curcol))
    )
  )

(defun ase-db-move-column-right ()
  (interactive)
  (let ((curcol (ase-db-get-current-column)))
    (ase-db-swap-columns curcol (1+ curcol))
    )
  )

(defun ase-db-swap-columns (column1 column2)
  ;; Swap the columns in the entries/rows
  (setq tabulated-list-entries
        (mapcar (function (lambda (tab-list)
                            (let
                                ;; Get the vector with content
                                ((vec (cadr tab-list)))
                              ;; Swap columns
                              (cl-rotatef (aref vec column1) (aref vec column2))
                              ;; Recreate the list with id and content vector
                              (list (car tab-list) vec)
                              )))
                tabulated-list-entries))
  ;; Swap the headings
  (cl-rotatef (aref tabulated-list-format column1)
              (aref tabulated-list-format column2))
    
  ;; Move the point
  (tablist-forward-column (- column2 column1))
  
  (tablist-save-marks
   ;; Print the content
   (tabulated-list-init-header)
   (tabulated-list-print t))
  )


;;Shortcut for ase-db on the currently marked file in dired
(require 'dired)
(add-hook 'dired-mode-hook
 (lambda ()
  (define-key dired-mode-map (kbd "C-c C-a")
    (lambda () (interactive) (ase-db (dired-get-filename))))
 ))

;; Functions to do some operations supported by tablist-operations-function
(defun ase-db-operations (operation &rest arguments)
  "Function for tablist OPERATION, it is called with the ARGUMENTS

The different functions take different number of arguments.  See
`tablist-operations-function' for more info.
`supported-operations' take no arguments and should return a list
of the other supported operations"
  (cl-ecase operation
    (supported-operations '(delete find-entry))
    (delete (cl-multiple-value-bind (ids) arguments (ase-db-delete ids)))
    (find-entry (cl-multiple-value-bind (id) arguments (ase-db-find-entry id ase-db-location)))
    )
  )

(defun ase-db-delete (ids)
  "This function will receive a list IDS.

It should then delete the ids by calling a python function and
then update `tabulated-list-entries'"
  (let ((to-delete-string (combine-and-quote-strings ids ",")))
    (message (concat "Deleting ID's: " to-delete-string))
    (ase-db-call-python-function-no-output (concat "delete(" to-delete-string ")"))
    (ase-db-set-tabulated-list-entries)
    ;; Should it update here?!? It doesn't read only delete..
    (ase-db-update-time-last-read)
    )
  )


(defun ase-db-ase-gui (ids)
  (ase-db-call-python-function-no-output (concat "view_rows("
                                                 ids
                                                 ",display='"
                                                 ase-db-gui-display
                                                 "')"))
  )

(defun ase-db-update-time-last-read ()
  (setq-local *ase-db-time-last-read* (current-time))  
  )

(defun ase-db-get-db-modification-time ()
  ;; TODO: Determine file or server based db
  (if (functionp 'file-attribute-modification-time)
      (file-attribute-modification-time (file-attributes ase-db-location))
    (nth 5 (file-attributes ase-db-location)))
  )

(defun ase-db-read (&optional force-db-to-python force-python-to-emacs no-check-modification-time)
  "Read the data from the database.

Reading from the database is done in parts:
1. From database to python
2. From python to Emacs

If the data has already been read, it compares the last
modification time of the db and the time of the last reading. We
can also FORCE-DB-TO-PYTHON a new read of the db to python or
FORCE-PYTHON-TO-EMACS to get the data from python, if we know it
is up-to-date there. In those cases we also need to set
NO-CHECK-MODIFICATION-TIME to non-nil."
  (if (and (not no-check-modification-time)
           (time-less-p *ase-db-time-last-read* (ase-db-get-db-modification-time)))
      ;; the time for last read is earlier than the last modification
      ;; time of the db -> we need to read in the data again
      (setq force-db-to-python t)
      )
  (if force-db-to-python
      (progn
        ;; data is read from db to python, i.e. it should also be read
        ;; from python to emacs
        (setq force-python-to-emacs t)
        
        ;; 1. Update data from db to python
        (if ase-db-debug
            (message "Updating dataframe from db"))
        (ase-db-call-python-function-no-output "update_dataframe_from_db()")
        )
    )
  (if force-python-to-emacs
      (progn
        ;; 2. Update data from python to emacs
        
        ;; Set the list header - tabulated-list-format
        (ase-db-set-tabulated-list-format)
        
        ;; Set the list entries - tabulated-list-entries
        (ase-db-set-tabulated-list-entries)

        ;; Update the time last read
        (ase-db-update-time-last-read)

        ;; 3. Update the data on screen

        (tablist-save-marks
         ;; Initialize the header line
         (tabulated-list-init-header)
        
         ;; Print all the entries in the buffer
         (tabulated-list-print t)
        )
        ;; Adjust the widths of the columns to fit the window
        (ase-db-adjust-widths)
        )
    (tablist-save-marks
     ;; Reprint to update age
     (tabulated-list-print t))
      )
  )


(defun ase-db-get-hidden-columns ()
  "Calls python to get the hidden columns."
  (ase-db-call-python-function "get_hidden_columns()")
  ;; Remove the trailing \n and split on ,
  (split-string (substring ase-db-output 0 -1) ",")
  )

(defun ase-db-get-marked-string ()
  "Get the marked items using `tablist-get-marked-items' and
  return them as a string separated by comma."
  (combine-and-quote-strings
   (mapcar #'(lambda (arg) (car arg))
           (tablist-get-marked-items))
   ",")
  )

(defun ase-db-get-current-column ()
  "Get the current column number.

 If before the first column return the last column."
  (if (tablist-current-column)
      (tablist-current-column)
    (car (last (tablist-major-columns))))
  )

;; Functions reached by key commands
(defun ase-db-update-data ()
  "Update the view of the data by forcing a read of the db."
  (interactive)
  (ase-db-read t)
  )

(defun ase-db-special-mark (query)
  "Mark the rows specified by the python search."
  (interactive (list (read-string "Mark using python query: ")))
  (ase-db-call-python-function (concat "get_ids_of_query('"
                                       query "')"))
  (ase-db-mark-ids (split-string (car (butlast (split-string ase-db-output "\n"))) ","))
  )

(defun ase-db-mark-ids (id-list)
  "Set a mark at all lines with ids in the ID-LIST"
  (setq ase-db-more-lines t)
  (save-excursion
    (goto-char (point-min))
    ;; Iterate through all rows
    (while ase-db-more-lines
      (let ((ids id-list))
        ;; Iterate through all ids
        (while ids
          (when (string= (car ids) (tabulated-list-get-id))
            (progn
              (tablist-put-mark)
              (setq ids nil)))
          (setq ids (cdr ids))
          )
        )
      (if (eq (forward-line 1) 1)
          ;; We are at the last line
          (setq ase-db-more-lines nil))))
  )

(defun ase-db-run-python-function (python-func)
  "Run the python function PYTHON-FUNC, and print the returned values.

This is mainly used for testing, to see the state in Python."
  (interactive (list (read-string "Run the Python function: ")))
  (ase-db-call-python-function python-func)
  (print ase-db-output)
  )

(defun ase-db-ase-gui-view (&optional id)
  "View the entry at the current point or the supplied ID using ase gui."
  (interactive)
  (ase-db-ase-gui (unless id (tabulated-list-get-id)))
  )

(defun ase-db-ase-gui-view-marked ()
  "View the marked entries with ase gui."
  (interactive)
  (ase-db-ase-gui (ase-db-get-marked-string))
  )

(defun ase-db-remove-column ()
  "Remove the current column from view."
  (interactive)
  
  ;; First remove the data in python
  (ase-db-call-python-function-no-output (concat
                                          "remove_key_from_selected_keys('"
                                          (tablist-read-column-name nil)
                                          "')"))
  
  (let ((curcol (ase-db-get-current-column)))
    ;; Delete the column in the entries/rows
    (setq tabulated-list-entries
          (mapcar (function (lambda (tab-list)
                              (let
                                  ;; Get the vector with content
                                  ((vec (cadr tab-list)))
                                
                                ;; Recreate the list with id and content vector
                                (list (car tab-list)
                                      ;; Remove the element at curcol
                                      (cl-remove (elt vec curcol) vec :start curcol :count 1))
                                )
                              ))
                  tabulated-list-entries))
    ;; Delete in the headings
    (setq tabulated-list-format
          (cl-remove (elt tabulated-list-format curcol) tabulated-list-format :start curcol :count))
    )
  
  (tablist-save-marks
   ;; Print the content
   (tabulated-list-init-header)
   (tabulated-list-print t))
  ;; Adjust the widths
  (ase-db-adjust-widths)
  )

(defun ase-db-add-column (c)
  (interactive (let ((candidates (ase-db-get-hidden-columns)))
        	 (list (completing-read "Add column: " candidates))))
  (ase-db-call-python-function-no-output (concat
                                          "add_key_to_selected_keys('"
                                          c
                                          "',"
                                          (number-to-string (1+ (ase-db-get-current-column)))
                                          ")"))
  (ase-db-read nil t t)
  )

(defun ase-db-add-data (data)
  "Add a data entry to the data dictionary."
  (interactive (list (read-string "Add data as 'entry=python expression': ")))
  (ase-db-call-python-function-no-output (concat "add_to_data("
                                                 (ase-db-get-marked-string)
                                                 ","
                                                 data
                                                 ")"))
  )

(defun ase-db-check-key-value-pair (kvp-string)
  "Check that the key value pair KVP-STRING is well-formed."
  (let ((kvp-str (string-trim kvp-string)))
    (unless (and (string-match-p (regexp-quote "=") kvp-str)
                 (not (or (string= "=" (substring kvp-str 0 1))
                          (string= "=" (substring kvp-str -1)))))
      (user-error "%s is not well formed. Use the format xx=yy" kvp-str)
      )
    
    )
  )

(defun ase-db-add-key-value-pair (kvp)
  "Add a number of key value pairs (KVP) to the current or selected row(s)."
  (interactive (list (read-string "Add key value pair(s): ")))
  ;; Check that the key value pair is well formed
  ;; Split upon , and then check in each element that a = exists
  (mapcar 'ase-db-check-key-value-pair (split-string kvp ","))
  (ase-db-call-python-function-no-output (concat "add_key_value_pair("
                                                 (ase-db-get-marked-string)
                                                 ","
                                                 kvp
                                                 ")"))
  (ase-db-read nil t t)
  )

(defun ase-db-export-to-csv ()
  "Export the view to csv but in a little different way than the tablist-export-csv function"
  (interactive)
  ;; Creating the buffer here as we then have access to it as a
  ;; variable
  (let ((csv-buffer (get-buffer-create (format "%s.csv" (buffer-name)))))
    (tablist-export-csv "," nil nil csv-buffer nil)
    (with-current-buffer csv-buffer
      (save-buffer)
      )
    )
  )

(defun ase-db-copy-into-other-db (other-db)
  "Copy the marked rows into another db"
  (interactive "FLocation of the other database: ")
  (ase-db-copy-rows (ase-db-get-marked-string) other-db)
  )

(defun ase-db-copy-rows (ids other-db)
  (ase-db-call-python-function-no-output (concat "copy_rows('"
                                                 other-db
                                                 "',"
                                                 ids ")"))
  )

(defun ase-db-export-to-traj (filename)
  "Export the marked rows into a trajectory file"
  (interactive "FTrajectory file: ")
  (ase-db-call-python-function-no-output (concat
                                          "export_to_traj('"
                                          filename
                                          "',"
                                          (ase-db-get-marked-string)
                                          ")"))
  )

;; Mark and filter functions
(defun ase-db-mark-filter (op column-name val)
  "Mark the rows as applied by the filter."
  (interactive (ase-db-read-filter))
  (tablist-map-with-filter 'tablist-put-mark `(,op ,column-name ,val))
  )

(defun ase-db-push-filter (op column-name 2nd-arg)
  "Add a new filter matching a predicate.

This could either be numeric, regexp or matching strings."
  (interactive (ase-db-read-filter))
  (tablist-push-filter `(,op ,column-name ,2nd-arg) (called-interactively-p 'any))
  )

(defun ase-db-read-filter ()
  "Mark the rows as applied by the filter."
  (let*
      ((column-name (completing-read (format "Mark regarding key (Default %s): " (tablist-read-column-name nil))
                                     (tablist-column-names) nil t nil nil (tablist-read-column-name nil)))
       (op (completing-read "Use operator (re and =~ are regexp, == are string searches): "
            '("=" "<" ">" "<=" ">=" "=~" "re" "==") nil t))
       val
       )

    (cond
     ((or (string= op "re") (string= op "=~"))
      ;; Regexp search
      (setq val (read-regexp (format "%s %s " column-name op)))
      (setq op "=~"))
     ((string= op "==")
      ;; String search
      (setq val (read-string (format "%s %s " column-name op))))
     (t
      ;; Numeric search
      (setq val (number-to-string
                 (read-number
                  (format "%s %s " column-name op))))      
      )
     )
    (setq op (intern op))
    (list op column-name val)
      )
  )

;; Functions for interaction with python
(defun ase-db-init-python ()
    ;; Check if there is a python instance running that we can use
    (if (not (setq pyproc (get-buffer-process "*ase-db-Python*")))
        ;; Python process is not started already - start it
        (progn
          (setq pyproc (start-process "python" "*ase-db-Python*" "python" "-i"))
          
          ;; We import here to be able to input data as np.arrays for example.
          (ase-db-do-python-no-output "import numpy as np; import pandas as pd\n")
          
          (ase-db-do-python-no-output (concat "import sys; sys.path.insert(0, '"
                                              ase-db-python-source-folder
                                              "')\n"))
          
          ;; Connect to the database using DatabaseConnection and put
          ;; the variable in db_dct with the ase-db-location as key
          ;; (process-send-string pyproc "from get_all_py import DatabaseConnection, db_dct \n")
          (ase-db-do-python-no-output "from get_all_py import DatabaseConnection, db_dct, start_connection \n")
          )
        )
    )


(defun ase-db-connect-to-database (address)
  ;; (process-send-string pyproc (concat "db_dct['" address "'] = DatabaseConnection('" address "')\n"))
  (ase-db-do-python-no-output (concat "db_dct['"
                                      address
                                      "'] = DatabaseConnection(fname='"
                                      address
                                      "', selection='"
                                      ase-db-selection
                                      "')\n"))
  )

(defun ase-db-do-python-no-output (pythonstring)
  (set-process-filter pyproc 'ase-db-filter-no-output)
  (process-send-string pyproc pythonstring)
  (accept-process-output pyproc 0 nil t)
  (message "READY")
  )

(defun ase-db-filter-no-output (proc outputstring)
  ;; kill string with no output
  (if ase-db-debug
      (message (concat "No output filter: " outputstring))
      )
  (setq ase-db-nonsense-output outputstring)
  )

(defun ase-db-do-python-with-output (pythonstring)
  ;; Setting a control variable that handles if it is ok to continue
  ;; i.e. if the full output has been received. ase-db-not-ready
  ;; should be set to nil in the filter function when the output is
  ;; finished
  (setq ase-db-not-ready t)
  (set-process-filter pyproc 'ase-db-filter-with-output)
  (process-send-string pyproc pythonstring)
  (setq ase-db-output-started nil)
  (setq ase-db-output "")
  (accept-process-output pyproc 0 nil t)
  (while ase-db-not-ready
    (message (concat "waiting for " pythonstring))
    (sleep-for .1)
    )
  (if (not ase-db-not-ready)
      (message "READY")
    (message "GAVE UP!!")
    )
  )

(defun ase-db-test-connection ()
  ;; In this first connection test we wait for the string ">>> " and
  ;; then accept output
  (setq ase-db-not-ready t)
  (set-process-filter pyproc 'ase-db-first-filter)
  (process-send-string pyproc (concat "db_dct['" ase-db-location "'].pass_test('TESTING CONNECTION')\n"))
  (setq ase-db-output-started nil)
  (setq ase-db-output "")
  (accept-process-output pyproc 0 nil t)
  (if ase-db-debug
      (let ((ase-db-slept-for 0))
        (while (and ase-db-not-ready (< ase-db-slept-for 7))
          (message "waiting for test")
          (sleep-for .1)
          (setq ase-db-slept-for (+ ase-db-slept-for .1))
          )
        )
    (progn
      (while ase-db-not-ready
        (message "waiting for test")
        (sleep-for .1)
        )
      )
    )
  ;; The connection is tested but also if the db is empty or the
  ;; selection will result in 0 rows. We will leave behind the empty
  ;; db because that is controlled by ase.
  (if (string= ase-db-output "EMPTY DB\n")
      (progn
        (append-to-file (concat "The db " ase-db-location " is empty!\n")
                        nil "/tmp/ase-db-tui-output")
        (ase-db-exit-with-output)
        )
      )
  (if (string= ase-db-output "TESTING CONNECTION\n")
      (message "CONNECTION WORKS")
    (progn
      (message "CONNECTION DOESN'T WORK")
      (print ase-db-output)
      )
      )
  )

(defun ase-db-first-filter (proc outputstring)
  (if ase-db-debug
      (progn
        (message "Output started: ")
        (print ase-db-output-started)
        (message "First filter:")
        (print outputstring)))
  (if (string= outputstring ">>> ")
      ;; After this the real output starts
      (setq ase-db-output-started t)
    (if ase-db-output-started
        ;; Concatenate outputstrings together
        (progn
          (if ase-db-debug
              ;; Are the end of the outputstring also '>>> ' on Niflheim?
              (progn
                (message "substring -4 to end: ")
                (print (substring outputstring -4 nil))
                (message "substring 0 to 4":)
                (print (substring outputstring 0 4))))
          (if (string= (substring outputstring -4 nil) ">>> ")
              ;; This was the last of the real output
              (progn
                (setq outputstring (substring outputstring 0 -4))
                (setq ase-db-output-started nil)
                (setq ase-db-not-ready nil)
                )
              )
          (if (string= (substring outputstring 0 4) ">>> ")
              ;; First line of output contains the prompt ">>> "
              ;; Here we remove this
              (setq outputstring (substring outputstring 4 nil))
              )
          (setq ase-db-output (concat ase-db-output outputstring)))
        )
    )
  )

(defun ase-db-filter-with-output (proc outputstring)
  ;; Remove >>> in front and after the transfered data
  (if ase-db-debug
      (message (concat "Filter with output: " outputstring)))
  (if (not (string= outputstring ">>> "))
      (progn
        (if (not ase-db-output-started)
            (if (or (string= (substring outputstring 0 9) "STARTING\n")
                    (string= (substring outputstring 0 13) ">>> STARTING\n"))
                ;; After this the real output starts
                (progn
                  (setq ase-db-output-started t)
                  (if (string= (substring outputstring 0 1) ">")
                      ;; The string starts with >>> and is 13 characters long
                      (setq outputstring (substring outputstring 13 nil))
                    ;; The string does not start with >>> and is 9 characters long
                    (setq outputstring (substring outputstring 9 nil))
                    )
                  )
              )
          )
        (if ase-db-output-started
            ;; Concatenate outputstrings together
            (progn
              (if (and (> (length outputstring) 3)
                       (string= (substring outputstring -4 nil) ">>> "))
                  ;; This was the last of the real output. The outputstring
                  ;; can be empty after removing STARTING then it isn't the
                  ;; last
                  (progn
                    (setq outputstring (substring outputstring 0 -4))
                    (setq ase-db-output-started nil)
                    (setq ase-db-not-ready nil)
                    )
                )
              (setq ase-db-output (concat ase-db-output outputstring)))
          )
        )
    ;; The outputstring is ">>> " thus it could be before the data
    ;; transfer has started or the end of the data
    (if ase-db-output-started
        ;; End of output
        (progn
          (setq outputstring (substring outputstring 0 -4))
          (setq ase-db-output-started nil)
          (setq ase-db-not-ready nil)
          )
      )
    )
  )

(defun ase-db-call-python-function (arg &optional loc)
  ;; Calls the function arg defined in the Python class DatabaseConnection
  ;; Remember ending parenthesis
  ;; It could also be called with (format "db_dct['%s'].%s\n" ase-db-location arg)
  (setq ase-db-output "")
  (unless loc (setq loc ase-db-location))
  (ase-db-do-python-with-output (concat "db_dct['" loc "']." arg "\n"))
  )

(defun ase-db-call-python-function-no-output (arg &optional loc)
  "Calls ARG defined in the Python class DatabaseConnection.
Remember ending parenthesis."
  (unless loc (setq loc ase-db-location))
  (ase-db-do-python-no-output (concat "db_dct['" loc "']." arg "\n"))
  )

(provide 'ase-db)
;;; ase-db.el ends here
