;;; ase-db.el --- Use ase db in emacs -*- lexical-binding: t -*-

;; Copyright (C) 2015 Steen Lysgaard

;; Author: Steen Lysgaard <st.lysgaard@gmail.com>
;; URL: Nowhere
;; Keywords: ase-db, Atomic Simulation Environment

;;; Commentary:
;;
;; Start it with M-x ase-db
;; then you will get prompted for the location of the db file.
;;
;; Enjoy!

;;; Commentary:

;; This library implements "entries" that is used when invoking find
;; entry on a row in the database. It shows more information about the
;; row and lets you edit stuff.

;;; Code:

(require 'eieio)

(defcustom ase-db-entry-separator "
===================================================================

"
  "The separator used for divide different types of entries.")

(defun ase-db-find-entry (id parent)
  "This function receives an ID and should display more info about that ID.

PARENT is the db thats this entry belongs to. It is being said so
that changes can be made to the db from the entry view."
  (setq ase-db-find-entry-buffer-name (concat "*id=" id "*"))
  (let ((plot-of-atoms (ase-db-get-plot-of-atoms id)))
    ;; Get the info before switching buffers to still have access to the
    ;; buffer local variables
    (ase-db-call-python-function (concat "get_all_info(" id ")"))
    ;; Put the plot of atoms in a window to the right maybe using
    ;; (split-window-right)
    (save-current-buffer
      (switch-to-buffer ase-db-find-entry-buffer-name)
      (let ((inhibit-read-only t)
            (fit-window-to-buffer-horizontally "only"))
        (setq ase-db-plot-window (split-window-right))
        (with-temp-buffer-window
         (concat "plot of atoms id: " id) nil nil
         (princ plot-of-atoms)
         )
        (fit-window-to-buffer ase-db-plot-window)
        (ase-db-find-entry-mode)
        (setq-local ase-db-entry-id id)
        (setq-local ase-db-entry-parent parent)
        (setq ase-db-edited-columns (list))
        (ase-db-update-entry)
      )
    )
    )
  )

(defun ase-db-update-entry ()
  (let ((inhibit-read-only t))
    (erase-buffer)
    (goto-char (point-min))
    (insert ase-db-entry-id ": \n")
    (goto-char (point-max))
    (ase-db-build-and-insert-sections
     (ase-db-split-sections-output ase-db-output))
    (goto-char (point-min))
    )
  
  )

(defun ase-db-get-plot-of-atoms (id)
  "Return the plot of atoms of row with ID"
  (ase-db-call-python-function (concat "get_plot_of_atoms(" id ")"))
  ase-db-output
  )

(define-derived-mode ase-db-find-entry-mode special-mode "ase-db-find-entry"
  "Major mode for ase-db-find-entry.
Letters do not insert themselves; instead, they are commands.
\\<ase-db-entry-mode-map>
\\{ase-db-entry-mode-map}"
  (use-local-map ase-db-entry-mode-map)
  (buffer-disable-undo)
  (hydra-ase-db-find-entry/body)
)

(defvar ase-db-entry-mode-map
  (let ((kmap (make-sparse-keymap)))
    (define-key kmap (kbd "C-n") (lambda () (interactive) (ase-db-move-cursor "down")))
    (define-key kmap (kbd "<down>") (lambda () (interactive) (ase-db-move-cursor "down")))
    (define-key kmap (kbd "C-p") (lambda () (interactive) (ase-db-move-cursor "up")))
    (define-key kmap (kbd "<up>") (lambda () (interactive) (ase-db-move-cursor "up")))
    (define-key kmap (kbd "<right>") (lambda () (interactive) (ase-db-move-cursor "right")))
    (define-key kmap (kbd "<left>") (lambda () (interactive) (ase-db-move-cursor "left")))
    (define-key kmap (kbd "C-f") (lambda () (interactive) (ase-db-move-cursor "right")))
    (define-key kmap (kbd "C-b") (lambda () (interactive) (ase-db-move-cursor "left")))
    kmap)
  )

(defhydra hydra-ase-db-find-entry (:color pink
                                          :hint nil
                                          ;; :after-exit (hydra-ase-db/body)
                                          )
  "
^Navigation^               ^Manipulation^               ^Return^
-----------------------------------------------------------------------------------------------------
_n_ext item                _e_dit section               _q_: go back
_p_revious item            add _k_ey-value pair
                         add to _d_ata
"
  ("q" ase-db-exit-entry :color blue)
  ("n" ase-db-next-section)
  ("p" ase-db-previous-section)
  ("e" ase-db-edit-section)
  ("k" ase-db-add-key-value-pair-in-entry)
  ("d" ase-db-entry-add-to-data)
  )

(defun ase-db-add-key-value-pair-in-entry (kvp)
  "Add a key-value pair defined in KVP."
  (interactive (list (read-string "Add key value pair(s): ")))
  ;; Test that the kvp-or-data is well-formed
  (mapcar 'ase-db-check-key-value-pair (split-string kvp ","))
  (ase-db-entry-call-python-function "add_key_value_pair" kvp)
  )

(defun ase-db-entry-add-to-data (data)
  "Add the DATA to the data dictionary."
  (interactive (list (read-string "Add data as 'entry=python expression': ")))
  (ase-db-entry-call-python-function "add_to_data" data)
  )

(defun ase-db-edit-section (kvp python-function)
  "Edit the current highlighted section."
  (interactive (let ((sec (car (ase-db-find-section-at-point (point)))))
                 (if (ase-db-entry-is-section-editable sec)
                     (list (read-string "Edit: " (replace-regexp-in-string "\\(: \\).*\\'" "=" (oref sec content) nil nil 1))
                           (oref sec editable))
                   (user-error "Not editable")
                   )
                 ))
  (ase-db-entry-call-python-function python-function kvp)
  (add-to-list 'ase-db-edited-columns (car (split-string kvp "=")))
  )

(defun ase-db-entry-call-python-function (python-function kvp-or-data)
  "Call the PYTHON-FUNCTION that inserts or changes the KVP-OR-DATA in the db."  
  ;; Update the key_value_pair in the db and df
  (ase-db-call-python-function-no-output (concat python-function "("
                                                 ase-db-entry-id
                                                 ","
                                                 kvp-or-data
                                                 ")")
                                         ase-db-entry-parent)
  ;; Get up-to-date info from db
  (ase-db-call-python-function (concat "get_all_info(" ase-db-entry-id ")")
                               ase-db-entry-parent)
  ;; Update the entry view
  (ase-db-update-entry)
    )

(defun ase-db-exit-entry ()
  "This function is called by `hydra-ase-db/tablist-find-entry'
  and takes care of exiting the find entry screen and restart the
  original hydra."
  (interactive)
  (delete-other-windows)
  (quit-window t)
  ;; Update the original view if any visible columns have been changed
  (ase-db-update-if-visible-changed)
  (hydra-ase-db/body)
  )

(defun ase-db-update-if-visible-changed ()
  "Update the view if any of the visible columns changed values."
  (while ase-db-edited-columns
    (if (seq-contains-p (tablist-column-names) (car ase-db-edited-columns))
        (progn
          (ase-db-read nil t t)
          (setq ase-db-edited-columns (list)))
      (setq ase-db-edited-columns (cdr ase-db-edited-columns))
        )
    )
  )

(defun ase-db-build-and-insert-sections (section-list)
  "Create sections from the SECTION-LIST and inserts them in the current buffer.

The first section will start at the current point."
  (setq ase-db-sections (list))
  
  (while section-list
    (let* ((ase-db-section-input (split-string (car section-list) "-EDITABLE-" t))
           (ase-db-content (car ase-db-section-input)))
      (if (string= ase-db-content "SEPARATOR")
          (insert ase-db-entry-separator)
        (progn
          ;; Check if some function for editing the value was passed
          (if (> (length ase-db-section-input) 1)
              (setq ase-db-editable (cadr ase-db-section-input))
            (setq ase-db-editable "")
              )
          (setq ase-db-start-point (point))
          (insert (concat ase-db-content "\n"))
          (setq ase-db-end-point (1- (point)))
          (setq ase-db-sections (append ase-db-sections
                                        (list (ase-db-section :content ase-db-content
                                                              :start ase-db-start-point
                                                              :end ase-db-end-point
                                                              :editable ase-db-editable))))
          ))
      )
    (setq section-list (cdr section-list))
    )
  
  )

;;; Navigation

(defun ase-db-move-cursor (direction)
  "Move the cursor in the DIRECTION up, down, left, right.

Highlighting is updated as needed."

  (ase-db-find-section-and-highlight t)
  ;; Move line
  (cond ((string= direction "up") (previous-line))
        ((string= direction "down") (next-line))
        ((string= direction "left") (backward-char))
        ((string= direction "right") (forward-char))
        )
  
  (ase-db-find-section-and-highlight)
  )

(defun ase-db-find-section-and-highlight (&optional remove-highlight)
  "Find the section at point and highlight it or remove the
  highlight if REMOVE-HIGHLIGHT is non-nil"
  ;; Find current section
  (let ((cur-sec (car (ase-db-find-section-at-point (point)))))
    (if (ase-db-section-p cur-sec)
        (if remove-highlight
            ;; Unhighlight it
            (ase-db-remove-overlay cur-sec)
          ;; Highlight
          (ase-db-make-overlay cur-sec)
            )))
  )

(defun ase-db-next-section (&optional move-to-previous)
  "Move to the next section highlight it and unhighlight the previous.

If MOVE-TO-PREVIOUS is non-nil the cursor will move to the previous instead"
  (interactive)
  ;; Find current section
  (let* ((secs (ase-db-find-section-at-point (point)))
         (cur-sec (car secs))
         (prev-sec (cadr secs))
         (next-sec (car (cddr secs))))
    (if (ase-db-section-p cur-sec)
        ;; Unhighlight that section
        (ase-db-remove-overlay cur-sec))
    ;; Move to next/previous section start and highlight it
    (if move-to-previous
        (progn
          (goto-char (oref prev-sec start))
          (ase-db-make-overlay prev-sec))
      (progn
        (goto-char (oref next-sec start))
        (ase-db-make-overlay next-sec)
        ))
    )
  )

(defun ase-db-previous-section ()
  "Move to the previous section highlight it and unhighlight the former highlighted section."
  (interactive)
  (ase-db-next-section t)
  )

(defun ase-db-find-section-at-point (p)
  "Return the section at point P."
  (let ((sections ase-db-sections)
        (found-section-p nil)
        (found-next-section-p nil)
        (found-previous-section-p nil)
        (i 0))

    (while (and (not found-section-p)
                (< i (length sections)))
      
      (setq cur-sec (nth i sections))
      
      ;; Test for next section: if the start of current section > p
      ;; for the first time it must be the next section
      (if (and (not found-next-section-p)
               (> (oref cur-sec start) p))
          (progn
            (setq next-sec cur-sec)
            (setq found-next-section-p t)
            ))
      
      ;; Test for previous section a similar way
      (if (and (not found-previous-section-p)
               (< p (oref cur-sec end)))
          (progn
            (if (eq i 0)
                (setq prev-sec (car (last sections)))
              (setq prev-sec (nth (1- i) sections))
              )
            (setq found-previous-section-p t)
            ))
          

      ;; Test for current section
      (when (and (<= p (oref cur-sec end))
                 (>= p (oref cur-sec start)))
        (setq found-section-p t)
        )
      (setq i (1+ i))
      )
    ;; (if (not found-next-section-p)
    (if found-section-p
        ;; Found current section then set next and previous are
        ;; trivial to determine. Test for value of i first
        (progn
          (if (eq i (length sections))
              ;; There are no further sections we must use the first
              (progn
                (setq next-sec (car sections))
                (setq prev-sec (nth (- i 2) sections))
                )
            (progn
              (setq next-sec (nth i sections))
              (if (eq i 1) ;; i will never be 0 since it is
                           ;; incremented in the end. The current
                           ;; section is apparently the first
                           ;; i.e. previous must be the last section
                  (setq prev-sec (car (last sections)))
                (setq prev-sec (nth (- i 2) sections)))))
          ))
    (if (not found-section-p)
        (setq cur-sec nil)
        )
    (list cur-sec prev-sec next-sec)
    )
  )

(defun ase-db-entry-is-section-editable (section)
  "Returns t if the editable part in the SECTION is not the empty
  string, nil otherwise."
  ;; if section is not editable the oref call should return ""
  (not (string= (oref section editable) ""))
  )

(defun ase-db-make-overlay (section)
  "Put an overlay on the region defined in SECTION.

The type of overlay should be highlight for editable and
  underline for non-editable."
  (let ((ov (make-overlay (oref section start) (oref section end))))
    (if (ase-db-entry-is-section-editable section)
        (overlay-put ov 'font-lock-face 'highlight)
      (overlay-put ov 'font-lock-face 'underline)
        )
    )
  (setq mark-active nil)
  )

(defun ase-db-remove-overlay (section)
  "Remove the overlay put on SECTION."
  (remove-overlays (oref section start) (oref section end))
  )

(defclass ase-db-section ()
  ((content  :initform ""
             :initarg :content
             :type string)
   (start    :initform 0
             :initarg :start
             :type integer)
   (end      :initform 0
             :initarg :end
             :type integer)
   (editable :initform ""
             :initarg :editable
             :type string)
   )
  "Define the parts that make up a section.")

(defun ase-db-split-sections-output (output)
  "Split the OUTPUT in sections.

The OUTPUT should contain the strings -START_SECTION- and
  -END_SECTION- to surround each data section."
  (let ((ss (split-string output "-END_SECTION-" nil "\n*-START_SECTION-")))
    (if (string= (car (last ss)) "\n")
        (nbutlast ss 1)
      )
    ss
    )
  )

(provide 'ase-db-entry)
;;; ase-db-entry.el ends here
