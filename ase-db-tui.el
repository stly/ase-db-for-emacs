;; INSTALL PACKAGES
;; --------------------------------------

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(use-package
     material-theme
     hydra
     server
     tablist
     ivy))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

(eval-when-compile
  (require 'use-package))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (tablist use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setq inhibit-startup-message t) ;; hide the startup message
(menu-bar-mode -1)  ;; Do not show the menu bar
(load-theme 'material t) ;; load material theme
(ivy-mode 1)
;;Do not ask yes or no but simply y or n
(defalias 'yes-or-no-p 'y-or-n-p)
(defun display-startup-echo-area-message ()
  (message "ase-db-tui is ready!"))

;; ASE-DB FOR EMACS
;; --------------------------------------
(add-to-list 'load-path "~/")
(load-library "ase-db")

;; (add-hook 'tty-setup-hook 'ase-db-open-emacsclient)
;; (add-hook 'find-file-hook 'ase-db-open-emacsclient)
;; (ase-db-process-command-line-args)


