from ase.db import connect
from ase.db.core import default_key_descriptions, float_to_time_string, now
from ase.visualize import view
from ase.io import Trajectory
from plot_atoms import plot
from collections import defaultdict
from functools import wraps
from multiprocessing import Pool
import numpy as np
import pandas as pd
import os
import json


def pass_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwds):
        print('STARTING')
        return func(*args, **kwds)
    return wrapper


def section_output(output, editable_function=None):
    start = "-START_SECTION-"
    end = "-END_SECTION-"
    editable = "-EDITABLE-"
    if editable_function is not None:
        editable += editable_function
    return start + output + editable + end


class DatabaseConnection(object):
    """Communicate with the database.

    Emacs should call any function and maybe receive output.

    """

    def __init__(self, fname,
                 hide_None_columns=True,
                 selection=""):
        super(DatabaseConnection, self).__init__()
        self.fname = os.path.abspath(fname)
        self.db = self._connect_to_db()

        self.hide_None_columns = hide_None_columns
        self.selection = selection

        self.columns = {}
        self.loaded_keys = False

        # Save the id of the last row in the database for later use
        self.final_id = next(self.db.select(sort='-id')).id
        # Determine the best number of cpus to use based on the size of the db
        # Add a cpu per 150 rows up to os.cpu_count()
        self.cpus = min(self.final_id // 150 + 1, os.cpu_count())

    def _connect_to_db(self):
        """Connect to a db and return the object."""
        return connect(self.fname)

    def _load_keys(self):
        """Load the keys to show from the saved keys file.

        We only want this to happen when the database connection is
        initiated, afterwards we can keep track of the keys without
        using the file.

        """
        if not self.loaded_keys:
            saved_keys = self.get_saved_keys()
            if saved_keys:
                self.selected_keys = saved_keys['selected_keys']
                self.excluded_keys = saved_keys['excluded_keys']
            else:
                self.selected_keys = []
                self.excluded_keys = ['unique_id']
            self.loaded_keys = True

    def update_dataframe_from_db(self):
        # populating the dataframe (self.df) will also set the userkeys
        self.populate_df_parallel()

        # Populate selected_keys maybe this should be moved to later
        self._load_keys()
        self.populate_all_keys()

    def populate_df_parallel(self):
        """Populate the dataframe (self.df) with all the data and set the
        userkeys (self.userkeys).

        The database is read in parallel using the multiprocessing
        module. Each processor is given an equal number of ids and
        read those. The division of ids is done based only on the size
        of the db. This could lead to some processors reading few rows
        and others many if the present ids are spread unevenly in the
        database.

        """
        pool = Pool(self.cpus)
        if self.cpus == 1:
            # 1 cpu -> Just select all
            sel_list = [""]
        else:
            # Divide the id selection between all processors
            ids = range(1, self.final_id + 1)
            n = int(self.final_id / self.cpus) + 1
            sel_list = []
            for chunk in [ids[i: i + n] for i in range(0, self.final_id, n)]:
                chunk_str = f'id>={chunk[0]}, id<={chunk[-1]}, {self.selection}'
                sel_list.append(chunk_str)

        # Collect data from all processors
        pm = pool.starmap(build_dataframe, zip([self.fname] * len(sel_list),
                                               sel_list))
        pool.close()
        pool.join()
        dfs, ukeys = zip(*pm)
        # Build the final dataframe
        df = pd.concat(dfs, sort=False)
        self.set_userkeys(set().union(*ukeys))
        self.df = df

    def set_userkeys(self, keys):
        self.userkeys = sorted(list(keys))

    def populate_all_keys(self):
        # userkeys = set()
        # for row in self.db.select(self.selection):
        #     userkeys.update(row.key_value_pairs)
        # self.set_userkeys(userkeys)
        default_columns = list(default_key_descriptions.keys())
        keys_in_db = default_key_order + [k for k in (self.userkeys
                                                      + default_columns)
                                          if k not in default_key_order]
        saved_keys = set(self.selected_keys + self.excluded_keys)
        new_keys = [k for k in keys_in_db if k not in saved_keys]
        if len(new_keys) > 0:
            for k in new_keys:
                self.add_key_to_selected_keys(k, remove_from_excluded=False)
        removed_keys = saved_keys - set(keys_in_db)
        if len(removed_keys) > 0:
            # The key has been removed from the db externally or the
            # key is not in any of the selected rows, thus it should
            # not go to the excluded keys
            for k in removed_keys:
                self.remove_key_from_selected_keys(k, False)

    def get_saved_keys(self):
        dct = self.get_saved_keys_dct()
        if self.fname in dct:
            return dct[self.fname]
        return None

    @pass_decorator
    def get_envvar(self, var):
        self.pass_to_emacs(os.environ[var])

    def get_key_file(self):
        # Use the environment variable that is set when invoking the
        # ase-db-tui command
        return os.environ['ASE_DB_TUI_SAVED_KEYS_FILE']

    def get_saved_keys_dct(self):
        keyfile = self.get_key_file()
        if (os.path.isfile(keyfile) and
                os.stat(keyfile).st_size > 0):
            with open(keyfile, 'r') as f:
                dct = json.load(f)
                return dct
        return {}

    def save_keys(self):
        dct = self.get_saved_keys_dct()
        dct[self.fname] = {'selected_keys': self.selected_keys,
                           'excluded_keys': self.excluded_keys}
        keyfile = self.get_key_file()
        with open(keyfile, 'w') as f:
            json.dump(dct, f)

    def remove_key_from_selected_keys(self, key, append_to_excluded=True):
        """Remove the key from the selected_keys list and add it to the
        excluded_keys."""
        if append_to_excluded:
            self.excluded_keys.append(key)
        self.selected_keys.remove(key)

    def add_key_value_pair(self, *ids, **kvp):
        # First update the db
        with self.db:
            for id in ids:
                self.db.update(id, **kvp)
        # Then the data, we don't need to reload the data
        for k, v in kvp.items():
            if k not in self.selected_keys:
                self.selected_keys.append(k)
            if k in self.excluded_keys:
                self.excluded_keys.remove(k)
            if k not in self.df.columns:
                self.df[k] = ''
            self.df[k] = self.df['id'].map(
                lambda x: v if x in ids else self.df.at[x, k])

    def add_to_data(self, *ids, **dct):
        # Form the input into the data format.

        # dct should hold a key will be the name of the entry in the
        # data dictionary. The value should be a python expression
        # that can be inserted directly as a python object. Maybe we
        # need some security here?!?
        data = {}
        for k, v in dct.items():
            data[k] = v

        # update the db, the view in main window does not show data so
        # no need to update the df. The view in entry should update
        # itself.
        with self.db:
            for id in ids:
                self.db.update(id, data=data)

    def translate_value_from_emacs(self, val):
        """Get a value from emacs and return it as the correct type.

        val is always string, return value should be int, float or string.
        """
        try:
            return int(val)
        except ValueError:
            try:
                return float(val)
            except ValueError:
                return val

    def add_key_to_selected_keys(self, key, position=None,
                                 remove_from_excluded=True):
        """Remove the key from the excluded_keys list and add it to
        self.selected_keys.
        """
        # Test for position is nil
        if position is None:
            self.selected_keys.append(key)
        else:
            self.selected_keys.insert(position, key)
        if remove_from_excluded:
            self.excluded_keys.remove(key)

    @pass_decorator
    def get_header(self):
        """Will print lines with all the keys, widths and how to sort that
        column.

        This function should be called before get_all_rows, because
        this function has the ability to exclude keys, e.g. if the
        column only contains None

        Example:
        # for id:
        >>> id, 3, <

        """

        keys_to_remove = []
        i = 0  # number of returned column names
        for k in self.selected_keys:
            # All data should be inserted in the df
            assert k in self.df.columns, f'{k} is not in columns!!!'
            # if k not in self.df.columns:
            #     self.set_column_data(k)
            if self.hide_None_columns and np.all(self.df[k].isna()):
                # All values in this column is None
                keys_to_remove.append(k)
                continue
            w = self.get_width_of_column(k)
            s = self.get_column_sort_type(k)
            self.pass_to_emacs([k, w, s, i])
            i += 1

        for k in keys_to_remove:
            self.remove_key_from_selected_keys(k)

    @pass_decorator
    def get_hidden_columns(self):
        """Will print the hidden columns for Emacs to see."""
        self.pass_to_emacs(self.excluded_keys)

    def get_width_of_value(self, val):
        if isinstance(val, str):
            return len(val)
        elif isinstance(val, float):
            if abs(val) > 1e6 or abs(val) < 1e-4:
                format_spec = "#.3g"
            else:
                format_spec = ".2f"
            value = '{1:{0}}'.format(format_spec, val)
        else:
            value = str(val)
        return len(value)

    def get_width_of_column(self, key):
        """Get the max length of the values connected with the key.

        The age is calculated in emacs, thus we just set it to 4."""
        if key == 'age':
            return 4
        maxlen = max(len(key), max(self.df[key].map(self.get_width_of_value)))
        return maxlen

    def get_column_sort_type(self, k):
        """Returns a string depending of the type of data in the column

        nil means not sortable
        """
        if k == 'age':
            return "age-sort"
        # Iterate over all actual values, discarding None and the like
        for v in self.df[k].dropna():
            # If a column has floats and Nones we still want the float
            # comparison
            try:
                float(v)
            except ValueError:  # None would probably need TypeError
                return "strings"
        return "numbers"

    @pass_decorator
    def get_ids_of_query(self, query):
        ids = []
        for row in self.db.select(query):
            ids.append(row.id)
        self.pass_to_emacs(ids)

    @pass_decorator
    def get_all_rows(self):
        """Pass the all the values to emacs, respecting the chosen order of
        keys."""
        for i, series in self.df.iterrows():
            series.fillna('', inplace=True)
            values = [series[k] for k in self.selected_keys]
            # or:
            # values = series[self.selected_keys].values
            self.pass_to_emacs(values)

    # def set_column_data(self, key):
    #     self.columns[key] = np.array([get_value(
    #         row, key) for row in self.db.select(self.selection)])

    def delete(self, *ids):
        if isinstance(ids, int):
            ids = [ids]
        # First delete the data in the db
        self.db.delete(ids)

        # Secondly delete the data in the df
        self.df = self.df.drop(np.ravel(ids), axis=0)

    def view_rows(self, *ids, display=':0.0'):
        os.environ['DISPLAY'] = display
        if isinstance(ids, int):
            ids = [ids]
        images = [self.db.get_atoms(id=i) for i in ids]
        view(images)

    def copy_rows(self, other_db_filename, *ids):
        if isinstance(ids, int):
            ids = [ids]
        with connect(other_db_filename) as other_db:
            for i in ids:
                other_db.write(self.db.get(id=i))

    def export_to_traj(self, fname, *ids):
        if isinstance(ids, int):
            ids = [ids]
        if os.path.exists(fname) and os.path.getsize(fname) > 0:
            traj = Trajectory(fname, mode='a')
        else:
            traj = Trajectory(fname, mode='w')
        for id in ids:
            traj.write(self.db.get_atoms(id=id))

    def get_all_keys(self):
        """Read all keys in the db."""
        if self.selected_keys is None:
            self.populate_all_keys()
        return self.selected_keys

    def print_all_keys(self):
        """Print all the keys (default + userkeys) to the screen."""
        selected_keys = self.get_all_keys()
        self.pass_to_emacs(selected_keys)

    @pass_decorator
    def get_all_info(self, id):
        row = self.db.get(id=id)
        so = section_output
        pte = self.pass_to_emacs
        # Add a function name signifying which python function to call
        # in order to edit the value or a string signifying that this
        # value cannot be changed
        pte(so(f"Formula: {row.formula}"))
        pte(so(f"Mass: {row.mass} {default_key_descriptions['mass'][2]}"))
        pte(so(f"Age: {get_value(row, 'age-section')}"))
        pte(so("SEPARATOR"))
        for k, v in row.key_value_pairs.items():
            v = format_value(v)
            pte(so(f"{k}: {v}", "add_key_value_pair"))
        pte(so("SEPARATOR"))
        for k, v in row.data.items():
            v = format_value(v)
            pte(so(f"{k}: {v}", "add_to_data"))

    @pass_decorator
    def get_plot_of_atoms(self, id):
        atoms = self.db.get_atoms(id=id)
        self.pass_to_emacs(plot(atoms))

    def pass_to_emacs(self, to_output):
        """Pass the output on to Emacs."""
        if isinstance(to_output, list):
            print(','.join(str(t) for t in to_output))
        elif isinstance(to_output, str):
            print(to_output)

    def pass_test(self, string_to_pass='CONNECTION WORKS'):
        if len(self.db) == 0:
            self.pass_to_emacs('EMPTY DB')
        else:
            self.pass_to_emacs(string_to_pass)


db_dct = {}
opened_times = {}
# id should always be first
default_key_order = ['id', 'age', 'user', 'formula', 'energy', 'pbc']

def build_dataframe(fname, sel=""):
    """Build a dataframe based on the given selection.

    The columns are built using defaultdicts, and put into a
    dataframe in the end.

    """
    cols = defaultdict(list)
    userkeys = set()
    keys = list(default_key_descriptions.keys())
    if 'pbc' not in keys:
        keys.append('pbc')
    i = 0
    db = connect(fname)
    for row in db.select(selection=sel):
        # default keys are always present
        for k in keys:
            cols[k].append(get_value(row, k))
        userkeys |= set(row.key_value_pairs.keys())
        for k in userkeys:
            cols[k].extend([pd.NaT] * (i - len(cols[k])) +
                           [get_value(row, k)])
        i += 1
    df = pd.DataFrame(cols, index=cols['id'])
    df['id'] = df['id'].astype('int')
    return df, userkeys


def get_value(row, key):
    """Get the value from the row to the dataframe."""
    if key == 'age-section':
        value = float_to_time_string(now() - row.ctime)
    elif key == 'age':
        value = '-AGE-' + str(row.ctime)
    elif key == 'pbc':
        value = ''.join('FT'[int(p)] for p in row.pbc)
    else:
        value = row.get(key, pd.NaT)
    return value


def start_connection(fname, **kwargs):
    abs_fname = os.path.abspath(fname)
    if abs_fname in db_dct:
        # database has already been opened, but is it up-to-date?
        if get_modified_time_of_db(abs_fname) < opened_times[abs_fname]:
            # db has been not modified since last read -> do not reread
            return
    db_dct[fname] = DatabaseConnection(fname=abs_fname, **kwargs)


def get_modified_time_of_db(fname):
    # get type of db
    # now only file based dbs work
    return os.stat(fname).st_mtime


def format_value(val, in_list=False):
    """Formatting the values of key value pairs and data to be presented
    in the entry view. Strings in lists are quoted by default, so we don't
    need to quote them here, but the rest we do as they will be easier to
    edit."""
    if isinstance(val, (int, float, bool, type(None), np.integer)):
        return val
    if isinstance(val, str):
        val = val.strip("'")
        if in_list:
            return "{val}".format(val=val)
        else:
            return "'{val}'".format(val=val)
    if isinstance(val, dict):
        dict_str = ["{0}: {1}".format(format_value(k), format_value(v))
                    for k, v in val.items()]
        return "{" + ', '.join(dict_str) + "}"
    if isinstance(val, (list, tuple)):
        return "{}".format([format_value(value, in_list=True)
                            for value in val])
    if isinstance(val, np.ndarray):
        return "np.array({})".format([format_value(value) for value in val])


if __name__ == '__main__':
    dc = DatabaseConnection('test.db')
    # print(dc.selected_keys)
    # dc.get_all_rows()
    dc.get_header()
    # dc.view_rows(2, 3)
    # dc.delete(2, 3)
    # dc.copy_rows('relaxed.db', 12, 13)


# (setq PyProc(start-process "python" "*ase-db-Python*" "python" "-i"))
# (process-send-string PyProc "a=1;b=2;c=a+b;print(c)\n")
# (set-process-filter PyProc 'my-filter)
# my-filter

# message

# (defun my-filter(proc string)
#  (message string)
#  )
