PREFIX?= /usr/local
BINDIR?= ${PREFIX}/bin
TUIHOME=${PREFIX}/.ase-db-tui
TESTDIR= ${TUIHOME}/test
INSTALL= install
INSTALLDIR= ${INSTALL} -d
INSTALLBIN= ${INSTALL} -m 755

uninstall:
	rm -rf ${TUIHOME}
	rm -f ${DESTDIR}${BINDIR}/ase-db-tui
	rm -f ${DESTDIR}${BINDIR}/ase-db-tui-kill
	rm -f ${DESTDIR}${BINDIR}/ase-db-tui-print

install:
	${INSTALLDIR} ${DESTDIR}${BINDIR}
	${INSTALLBIN} ase-db-tui ase-db-tui-kill ase-db-tui-print ${DESTDIR}${BINDIR}
	${INSTALLDIR} ${TUIHOME}
	${INSTALLDIR} ${TUIHOME}/.emacs.d
	${INSTALLDIR} ${TESTDIR}
	${INSTALL} ase-db.el ase-db-entry.el *.py ${TUIHOME}
	${INSTALL} ase-db-tui.el ${TUIHOME}/.emacs.d/init.el
	sed -i 's,##TUIHOME##,TUIHOME=${PREFIX},g' ${DESTDIR}${BINDIR}/ase-db-tui
	${INSTALL} test/*.py test/ase-db-tui-test ${TESTDIR}
	sed -i 's,##TUIHOME##,TUIHOME=${PREFIX},g' ${TESTDIR}/ase-db-tui-test

.PHONY: install uninstall

