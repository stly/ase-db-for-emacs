from ase.build import bulk
from itertools import product
from ase.db import connect
import sys

db = connect(sys.argv[1])
for size in product(range(1, 4), repeat=3):
    db.write(bulk('Au') * size)
