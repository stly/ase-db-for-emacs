from ase.db import connect
from ase.build import bulk

db = connect('test.db')
db.write(bulk('Al') * (2, 2, 2))
